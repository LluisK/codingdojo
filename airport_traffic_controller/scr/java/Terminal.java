package Mediator;

public class Terminal
{
    public static void main(String[] args)
    {
        TrafficController trafficController = new TrafficController();

        AirPlane airbus300 = new Airbus300("Airbus 300", trafficController);
        AirPlane antonov255 = new Antonov255("Antonov 255", trafficController);
        AirPlane boeing373 = new Boeing373("Boeing 373", trafficController);


        setAltitude(airbus300, 1500);
        setAltitude(antonov255, 3100);
        setAltitude(boeing373, 4105);
        setAltitude(airbus300, 2500);
        setAltitude(antonov255, 4200);
        setAltitude(airbus300, 1500);
    }

    private static void setAltitude(AirPlane airPlane, int feet)
    {
        try
        {
            System.out.println("Airplane "
                    .concat(airPlane.getAirPlaneName()
                            .concat(" at ")
                            .concat(Integer.toString(feet).
                                    concat(" feet"))));
            airPlane.setAltitude(feet);

            Thread.sleep(2000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}

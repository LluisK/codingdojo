package Mediator;

public interface AirTrafficControl
{
    void registerAirPlane(AirPlane airPlane);
    void receiveHeightAirPlane(AirPlane location);
}

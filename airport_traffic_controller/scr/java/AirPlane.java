package Mediator;

public class AirPlane
{
    private AirTrafficControl airTrafficControl;
    private String airPlaneName;
    private int altitude;

    protected AirPlane(String airPlaneName, AirTrafficControl airTrafficControl)
    {
        this.airTrafficControl = airTrafficControl;
        this.airPlaneName = airPlaneName;
        this.airTrafficControl.registerAirPlane(this);
    }

    public void setAltitude(int altitude)
    {
        this.altitude = altitude;
        airTrafficControl.receiveHeightAirPlane(this);
    }

    public int getAltitude() { return altitude;}

    public String getAirPlaneName() { return airPlaneName; }

    public void climb(int heightToClimb)
    {
        altitude += heightToClimb;
    }

    public void warnOfAirspaceIntrusionBy(AirPlane reportingAirPlane, AirPlane airPlane)
    {
        StringBuilder stringBuilderMessage = new StringBuilder();
        stringBuilderMessage.append("Be careful ".concat(reportingAirPlane.getAirPlaneName()));
        stringBuilderMessage.append(". You are so close to ".concat(airPlane.getAirPlaneName()));
        stringBuilderMessage.append(". You have been pushed to ".concat(Integer.toString(this.getAltitude())));
        stringBuilderMessage.append(" feet.");

        System.out.println(stringBuilderMessage.toString());
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj.getClass().equals(this.getClass()))
        {
            return false;
        }

        var incoming = (AirPlane)obj;
        return this.airPlaneName.equals(incoming.airPlaneName);
    }

    @Override
    public  int hashCode()
    {
        return airPlaneName.hashCode();
    }
}

package ObserverPattern;

public class FlightNotification extends Notification
{
    private final String description;
    private String arrival;

    public FlightNotification(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public String getArrival()
    {
        return arrival;
    }

    public void setArrival(String arrival)
    {
        this.arrival = arrival;
        broadcast();
    }
}

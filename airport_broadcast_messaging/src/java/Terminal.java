package ObserverPattern;

public class Terminal
{
    public static void main(String[] args)
    {
        String descriptionFlight = "Flight 98765 from Barcelona";

        FlightNotification flightNotificationFromBarcelona = new FlightNotification(descriptionFlight);

        //Screen A is subscribed to Barcelona flight
        AirportScreen screenA = new AirportScreen(flightNotificationFromBarcelona, "Screen A");

        //Barcelona flight communicates its arrival
        flightNotificationFromBarcelona.setArrival("Estimated arrival at 16:00");
        timeToUpdateInformation();

        //Barcelona flight communicates its delay
        flightNotificationFromBarcelona.setArrival("Arrival delayed at 16:30");
        timeToUpdateInformation();

        //Screen B is subscribed to Barcelona flight
        AirportScreen screenB = new AirportScreen(flightNotificationFromBarcelona, "Screen B");

        //Barcelona flight communicates its delay
        flightNotificationFromBarcelona.setArrival("Arrival delayed at 17:00");
        timeToUpdateInformation();

        //Screen A is unsubscribed to Barcelona flight
        screenA.removeFlight();

        //Barcelona flight communicates its delay
        flightNotificationFromBarcelona.setArrival("Arrival delayed at 19:00");
        timeToUpdateInformation();
    }

    private static void timeToUpdateInformation()
    {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

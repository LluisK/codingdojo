package ObserverPattern;

public interface Screen
{
    void update();
}
